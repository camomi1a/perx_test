module.exports = api => {
  const presets = ['@babel/preset-env', '@babel/preset-react'];
  const plugins = [ '@babel/plugin-proposal-class-properties' ];

  api.cache.never();

  return { presets, plugins };
};
