const webpack = require('webpack');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const TerserJSPlugin = require('terser-webpack-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const DotEnv = require('dotenv-webpack');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
const path = require('path');
const publicPath = '/';

module.exports = {
  mode: process.env.NODE_ENV === 'production' ? 'production' : 'development',
  optimization: {
    usedExports: true,
    minimize: process.env.NODE_ENV === 'production',
    runtimeChunk: 'single',
    concatenateModules: true,
    splitChunks: {
      cacheGroups: {
        vendor: {
          test: /[\\/]node_modules[\\/]/,
          name: 'vendors',
          chunks: 'all',
          filename: '[name].[contenthash].bundle.js',
        },
      },
    },
    minimizer: [
      new TerserJSPlugin({
        terserOptions: {
          compress: {
            // eslint-disable-next-line
              drop_console: true
          },
          output: {
            comments: false,
          },
        },
      }
      ), new OptimizeCSSAssetsPlugin({}),
    ],
  },
  entry: './src/App/client.js',
  module: {
    rules: [
      {
        test: /\.(jpg|png|gif|svg)$/,
        loader: 'image-webpack-loader',
        // Specify enforce: 'pre' to apply the loader
        // before url-loader/svg-url-loader
        // and not duplicate it in rules with them
        enforce: 'pre',
      },
      {
        test: /\.(png|svg|jpe?g|gif)$/i,
        loader: 'file-loader',
        options: {
          name() {
            if (process.env.NODE_ENV === 'development') {
              return '[path][name].[ext]';
            }

            return '[contenthash].[ext]';
          },
          publicPath: '/images',
          outputPath: 'images',
        },
      },
      {
        test: /\.(pdf)$/i,
        loader: 'file-loader',
        options: {
          name() {
            return '[name].[contenthash].[ext]';
          },
          publicPath: '/docs',
          outputPath: 'docs',
        },
      },
      {
        test: /\.(js|jsx)$/,
        include: [
          path.resolve(__dirname, 'src'),
        ],
        use: [
          {
            loader: 'babel-loader',
            options: {
              plugins: [ ['import', { 'libraryName': 'antd' } ] ],
              presets: [
                ['@babel/env', { exclude: [ 'transform-regenerator' ] } ],
              ],
            },
          },
        ],
      },
      {
        test: /\.css$/,
        exclude: /node_modules/,
        use: [
          MiniCssExtractPlugin.loader,
          {
            loader: 'css-loader',
            options: {
              url: true,
              sourceMap: process.env.NODE_ENV === 'development',
            },
          },
        ],
      },
      {
        test: /\.less$/,
        use: [
          MiniCssExtractPlugin.loader,
          {
            loader: 'css-loader',
            options: {
              url: true,
              sourceMap: process.env.NODE_ENV === 'development',
            },
          },
          { loader: 'less-loader', options: { javascriptEnabled: true } },
        ],
      },
      {
        test: /\.(ttf|eot|otf|woff|woff2)$/,
        loader: 'file-loader',
        options: {
          publicPath: '/fonts',
          outputPath: 'fonts',
        },
      },
    ],
  },
  resolve: {
    extensions: ['*', '.js', '.jsx'],
  },
  output: {
    publicPath,
    filename: '[name].[hash].js',
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: './src/index.html',
    }),
    new webpack.LoaderOptionsPlugin({ options: {} }),
    new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),
    new MiniCssExtractPlugin({
      filename: 'styles.[contenthash].css',
    }),
    new CleanWebpackPlugin(),
    new DotEnv({
      defaults: true,
    }),
    new webpack.EnvironmentPlugin({
      NODE_ENV: 'development',
    }),
    new BundleAnalyzerPlugin({ analyzerMode: 'disabled', openAnalyzer: process.env.NODE_ENV === 'production' }),
  ],
  devtool: process.env.NODE_ENV !== 'production' ? 'source-map' : false,
  devServer: {
    contentBase: '/',
    hot: process.env.NODE_ENV !== 'production',
    historyApiFallback: true,
    inline: true,
    port: '8800',
    host: 'localhost',
  },
};
