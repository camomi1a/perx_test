import App from '../App/App';
import Layout from '../App/Layout';
import Catalog from '../components/Catalog';
import Cart from '../components/Cart';

const routes = [
  {
    component: App,
    path: '/',
    routes: [
      {
        path: '/',
        component: Layout,
        routes: [
          {
            path: '/catalog/',
            component: Catalog,
            routes: [],
          },
          {
            path: '/cart/',
            component: Cart,
            routes: [],
          },
        ],
      },
    ],
  },
];

export default routes;
