import toConstantCase from 'to-constant-case';

export default function asyncAction(params) {
  const type = toConstantCase(params.type);

  function actionStarted() {
    return { type: `${type}_STARTED` };
  }

  function actionFinished(successData) {
    return { type: `${type}_FINISHED`, payload: successData };
  }

  function actionError(errors) {
    return { type: `${type}_ERROR`, payload: errors };
  }

  return (dispatch) => {
    dispatch(actionStarted());

    return params.actionPromise(params.payload)
      .then(data => dispatch(actionFinished(data)))
      .catch(errs  => dispatch(actionError(errs)));
  };
}

