import { applyMiddleware, combineReducers, compose, createStore } from 'redux';
import thunk from 'redux-thunk';
import { connectRouter, routerMiddleware } from 'connected-react-router';
import { createBrowserHistory } from 'history';
import { getCartReducer } from '../components/Cart/reducers';
import { getCatalogReducer } from '../components/Catalog/reducers';
import { catalogMiddleware } from '../components/Catalog/middlwares';
import { cartMiddleware } from '../components/Cart/middlwares';
import { filtersMiddleware } from '../components/Filters/middlwares';
import { getFiltersReducer } from '../components/Filters/reducers';

export const history = createBrowserHistory();

export default function (initialState = {}) {
  const rootReducer = combineReducers({
    router: connectRouter(history),
    catalog: getCatalogReducer,
    cart: getCartReducer,
    filters: getFiltersReducer,
  });

  const mdlwr =
    compose(applyMiddleware(
      thunk,
      routerMiddleware(history),
      catalogMiddleware,
      cartMiddleware,
      filtersMiddleware
    ));

  return createStore(rootReducer, initialState, mdlwr);
}
