
function getAjaxPromise(params) {
  return  fetch(params.url, {
    // headers: {
    //   'Content-Type': 'application/json',
    //   'Accept': 'application/json',
    //   ...params.headers,
    // },
    // credentials: 'include',
    method: params.method,
    body: JSON.stringify(params.data),
  });
}

export function get(params) {
  return getAjaxPromise({ method: 'GET', ...params });
}

export function post(params) {
  return getAjaxPromise({ method: 'POST', ...params });
}

export function patch(params) {
  return getAjaxPromise({ method: 'PATCH', ...params });
}

export function put(params) {
  return getAjaxPromise({ method: 'PUT', ...params });
}

export function remove(params) {
  return getAjaxPromise({ method: 'DELETE', ...params });
}

