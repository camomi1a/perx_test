import { get } from './http';
import queryString from 'query-string';

const baseUrl = process.env.API_URL;

export function apiGetGoods(search) {
  let locationSearch = '';

  if (search) locationSearch = `?${typeof (search) === 'string' ? search : queryString.stringify(search, { arrayFormat: 'comma' })}`;

  return get({ url: `${baseUrl}goods/${locationSearch}` }).then(response => response.json());
}

export function apiGetFilters() {
  return get({ url: `${baseUrl}dealers/` }).then(response => response.json());
}
