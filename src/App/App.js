import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { renderRoutes } from 'react-router-config';
// import './App.less';

class App extends Component {
  static propTypes = {
    route: PropTypes.object,
    location: PropTypes.object,
  };

  static defaultProps = {
    route: {},
    location: {},
  };

  render() {
    return (
      <div className='App'>
        {renderRoutes(this.props.route.routes)}
      </div>
    );
  }
}

export default App;
