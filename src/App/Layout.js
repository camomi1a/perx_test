import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { renderRoutes } from 'react-router-config';
import Footer from '../components/Footer';
import Header from '../components/Header';

import CssBaseline from '@material-ui/core/CssBaseline';
import Container from '@material-ui/core/Container';

const propTypes = {
  route: PropTypes.object,
  location: PropTypes.object,
};

class Layout extends Component {
  render() {
    return (
      <div className='layout-with-header-footer'>
        <Header/>
        <CssBaseline />
        <Container fixed  style={styles.wrapper}>
          {renderRoutes(this.props.route.routes)}
        </Container>
        <Footer/>
      </div>
    );
  }
}

const styles = {
  wrapper:{
    marginBottom: '32px',
  },
};

Layout.propTypes = propTypes;

export default Layout;
