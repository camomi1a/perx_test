import React from 'react';
import ReactDOM from 'react-dom';
import { createBrowserHistory } from 'history';
import { renderRoutes } from 'react-router-config';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'connected-react-router';
import configureStore from '../redux/configureStore';
import routes from '../router/routes';

const state = window.STORE_DATA;

delete window.STORE_DATA;
export const history = createBrowserHistory();

const store = configureStore(state);

const component = (
  <Provider store={store}>
    <ConnectedRouter history={history}>
      {renderRoutes(routes)}
    </ConnectedRouter>
  </Provider>
);

ReactDOM.render(component, document.getElementById('react-view'));
