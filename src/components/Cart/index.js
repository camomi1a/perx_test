import React from 'react';
import { PropTypes } from 'prop-types';
import { useSelector, useDispatch } from 'react-redux';
import CatalogProduct from '../CatalogProduct';
import DeleteIcon from '@material-ui/icons/Delete';
import { makeStyles } from '@material-ui/core/styles';
import { deleteProductFromCartAction, clearCartAction } from './actions';
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles({
  root: {
    margin: 8,
    maxWidth: 645,
    '&:hover':{
      boxShadow: '0 1px 3px 0 rgba(0,0,0,.85)',
    },
  },
  margin:{
    margin: 8,
  },
  img:{
    width: 240,
  },
});

export default function Cart() {
  const cart = useSelector(state => state.cart);
  const dispatch = useDispatch();

  function deleteItem(item) {
    dispatch(deleteProductFromCartAction(item));
  }

  return (
    <div>
      {!!cart.products.length
        ? <>
          <Typography onClick={() => dispatch(clearCartAction())}> Удалить все</Typography>
          {(cart.products || []).map((item) => {
            return (
              <CatalogProduct componentClasses={useStyles} key={item.name} product={item} contentStyle={{ display: 'flex' }}>
                <DeleteIcon onClick={deleteItem.bind(this, item)}/>
              </CatalogProduct>
            );
          })}</>
        : <div>Корзина пуста</div>}
    </div>
  );
}

Cart.propTypes = {
  cart: PropTypes.object,
};
