import { getCart, postCart } from './constants';

const cartInitialState = localStorage.cart ? JSON.parse(localStorage.cart) : { timeStamp: Date.now(), products:[], counter: 0 };

export function getCartReducer(state = cartInitialState, action) {
  switch (action.type) {
    case `${postCart}_FINISHED`: return { ...state, ...action.payload };
    case getCart: return { ...state };
    default: return  state;
  }
}
