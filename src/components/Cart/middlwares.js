import asyncAction from '../../redux/actions-factory';
import { apiGetCart, apiPostCart } from './promises';
import { addProductToCart, getCart, postCart, clearCartConst, removeProductFromCart, deleteProductFromCartConst } from './constants';

export const cartMiddleware = store => next => action => {

  switch (action.type) {
    case getCart : store.dispatch(asyncAction({ ...action, actionPromise: apiGetCart })); break;

    case postCart : store.dispatch(asyncAction({ ...action, actionPromise: apiPostCart })); break;

    case `${postCart}_FINISHED` : localStorage.cart = JSON.stringify(action.payload); break;

    case addProductToCart : {
      const products = [ ...store.getState().cart.products ];
      const productIndex = products.findIndex(product => action.payload.id === product.id);

      if (!~productIndex) products.push({...action.payload, quantity: 1} );// eslint-disable-line
      else products[productIndex].quantity = products[productIndex].quantity + 1;
      store.dispatch({ type: postCart, payload: getCartObject(products) });
    } break;

    case removeProductFromCart : {
      const products = [ ...store.getState().cart.products ];
      const productIndex = products.findIndex(product => action.payload.id === product.id);

      if (products[productIndex] && products[productIndex].quantity > 0) products[productIndex].quantity = products[productIndex].quantity - 1;// eslint-disable-line
      else products.splice(productIndex, 1);
      store.dispatch({ type: postCart, payload: getCartObject(products) });
    } break;

    case deleteProductFromCartConst : {
      const products = [ ...store.getState().cart.products ];
      const productIndex = products.findIndex(product => action.payload.id === product.id);

      products.splice(productIndex, 1);
      store.dispatch({ type: postCart, payload: getCartObject(products) });
    } break;

    case clearCartConst: store.dispatch({ type: postCart, payload: getCartObject([]) });break;
  };
  return next(action);
};

function getCounter(products) {
  return products.reduce((result, cur) => result + cur.quantity, 0);
}

function getCartObject(products) {
  return {
    products,
    counter:getCounter(products),
    timeStamp: Date.now(),
  };
}
