import { addProductToCart, removeProductFromCart, deleteProductFromCartConst, clearCartConst } from './constants';

export function addProductToCartAction(product) {
  return { type: addProductToCart, payload: product };
}
export function removeProductFromCartAction(product) {
  return { type: removeProductFromCart, payload: product  };
}
export function deleteProductFromCartAction(product) {
  return { type: deleteProductFromCartConst, payload: product  };
}

export function clearCartAction() {
  return { type: clearCartConst, payload: []  };
}
