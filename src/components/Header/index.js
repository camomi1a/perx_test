import React from 'react';
import { PropTypes } from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Typography from '@material-ui/core/Typography';
import Toolbar from '@material-ui/core/Toolbar';
import Badge from '@material-ui/core/Badge';
import IconButton from '@material-ui/core/IconButton';
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';
import MenuItem from '@material-ui/core/MenuItem';
import { Link } from 'react-router-dom';
import Container from '@material-ui/core/Container';
import { useSelector } from 'react-redux';


const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    marginBottom: theme.spacing(4),

  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
  headerItems: {
    color: '#fff',
    marginRight: theme.spacing(2),
  },
}));

export default function Header() {
  const cartCounter = useSelector(state => state.cart.counter);
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <AppBar position='static'>
        <Container fixed>
          <Toolbar>
            <Link to={'/catalog/'} className={classes.headerItems}>
              <MenuItem>
                Каталог
              </MenuItem>
            </Link>
            <Typography variant='h6' className={classes.title} />
            <Link to={'/cart/'}>
              <IconButton className={classes.headerItems}>
                <Badge badgeContent={cartCounter} color='secondary'>
                  <ShoppingCartIcon />
                </Badge>
              </IconButton>
            </Link>
          </Toolbar>
        </Container>
      </AppBar>
    </div>
  );
}

Header.propTypes = {
  cartCounter: PropTypes.number,
};
