import { getCatalogConst } from './constants';

export function getCatalogAction(filters) {
  return ({ type: getCatalogConst, payload: filters });
}
