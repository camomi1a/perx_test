import { getCatalogConst } from './constants';

const catalogInitialState = { products:[] };

export function getCatalogReducer(state = catalogInitialState, action) {
  switch (action.type) {
    case `${getCatalogConst}_FINISHED`: return { ...state, products: action.payload };
    default: return  state;
  }
}
