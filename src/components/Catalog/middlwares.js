import { getCatalogConst } from '../Catalog/constants';
import asyncAction from '../../redux/actions-factory';
import { apiGetGoods as apiGetCatalog } from '../../api/api';

export const catalogMiddleware = store => next => action => {
  switch (action.type) {
    case getCatalogConst : store.dispatch(asyncAction({
      ...action,
      actionPromise: apiGetCatalog,
    })); break;
    case `${getCatalogConst}_FINISHED` : {
      action.payload.forEach((product) => {
        product.id = product.name;
        product.title = product.name;
      });
    } break;
  }

  return next(action);
};

