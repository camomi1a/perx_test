import { catalogData } from './constants';

export const apiGetCatalog = (filters) => new Promise((resolve) => {
  setTimeout(() => {
    resolve(!Object.keys(filters).length ? catalogData : catalogData.filter((product) => {
      for (const key in filters) {
        return product[key].some((f) => {
          return !!~filters[key].indexOf(f);
        });
      }
    }));
  }, 300);
});

