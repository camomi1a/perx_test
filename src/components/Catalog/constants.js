export const getCatalogConst = 'GET_CATALOG';
export const updateCatalogConst = 'UPDATE_CATALOG';


export const catalogData = [
  { id: '1',
    quantity: 0,
    price: 9999.99,
    title: 'Удивленный Кот',
    image: 'https://img.huffingtonpost.com/asset/5dcc613f1f00009304dee539.jpeg?cache=QaTFuOj2IM&ops=crop_834_777_4651_2994%2Cscalefit_720_noupscale&format=webp',
    catPropertyes: ['hair', 'fat'],
  },
  { id: '2',
    quantity: 0,
    price: 999.99,
    title: 'Спящий кот',
    image: 'https://cdn1.fullpicture.ru/wp-content/uploads/2014/12/Samyj-urodlivyj-spyashhij-kot-v-YAponii-2.jpg',
    catPropertyes: ['hair', 'sleep', 'fat'],
  },
  { id: '3',
    quantity: 0,
    price: 99.99,
    title: 'Лысый кот',
    image: 'https://www.meme-arsenal.com/memes/92d7f3eeca4ad679cd1e40ae813c45f9.jpg',
    catPropertyes: [],
  },
  { id: '4',
    quantity: 0,
    price: 89.99,
    title: 'Котеночек',
    image: 'https://www.my-sfinks.ru/photo/img/kot-sfinks-spit-6.jpg',
    catPropertyes: ['sleep',  'sweet'],
  },
];
