import React, { Component } from 'react';

const style = {
  textAlign: 'center',
  padding:32,
  backgroundColor:'#fff',
};

class Footer extends Component {
  render() {
    return (
      <div style={style}>Тестовое задание выполнено Мартыновой Людмилой</div>
    );
  }
}

export default Footer;
