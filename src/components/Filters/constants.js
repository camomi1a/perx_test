export const applyFiltersConst = 'APPLY_FILTERS';
export const getFiltersConst = 'GET_FILTERS';

export const filters = [
  {
    title: 'пушистенький',
    value: 'hair',
  },
  {
    title: 'спит',
    value: 'sleep',
  },
  {
    title: 'милый',
    value: 'sweet',
  },
  {
    title: 'толстенький',
    value: 'fat',
  },
];
