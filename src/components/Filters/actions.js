import { applyFiltersConst, getFiltersConst } from './constants';

export function applyFiltersAction(filters) {
  return ({ type: applyFiltersConst, payload: filters });
}

export function getFiltersAction() {
  return ({ type: getFiltersConst, payload: {} });
}
