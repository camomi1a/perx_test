import { getFiltersConst } from './constants';

const filtersInitialState = { filters:{ dealers:[] }, timeStamp: Date.now() };

export function getFiltersReducer(state = filtersInitialState, action) {
  switch (action.type) {
    case `${getFiltersConst}_FINISHED`: return { ...state, filters:{ dealers: action.payload } };
    default: return  state;
  }
}
