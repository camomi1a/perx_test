import React, { useEffect, useState } from 'react';
import { connect, useDispatch, useSelector } from 'react-redux';
import { applyFiltersAction, getFiltersAction } from './actions';
import queryString from 'query-string';
import Filter from './component';

function Filters() {
  const storeFilters = useSelector(state => state.filters.filters.dealers);
  const [filters, setFilters] = useState(storeFilters);
  const applyedFilters = useSelector(state => queryString.parse(state.router.location.search));
  const dispatch = useDispatch();


  useEffect(() => {
    dispatch(getFiltersAction());
  }, []);

  useEffect(() => {
    const f = storeFilters.map((filter) => {
      for (const key in applyedFilters) {
        filter.checked = !!~applyedFilters[key].indexOf(filter.value);
      }
      return filter;
    });

    setFilters(f);
  }, [ storeFilters ]);

  function handleChange(filter) {
    dispatch(applyFiltersAction(filter));
  }

  return (
    <Filter filters={filters} onChange={handleChange.bind(this)} name={'dealers'}/>
  );
}

export default connect()(Filters);
